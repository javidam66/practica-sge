# -*- coding:utf-8 -*-

from flask import Flask, request, render_template, \
    redirect

# Settings
from settings import SETTINGS as Settings

app = Flask(__name__)

@app.route('/')
def home():
    return "Hola mundo"

def run_application(debug = False):
    print(u"Running application")
    # TODO: First run functions
    while True:
        # TODO: Checkers
        pass
call_app = raw_input(u"Lanzar Server (S) o App (A) ?: ")

while call_app.lower() != 's' and call_app.lower() != 'a':
    call_app = raw_input(u"Lanzar Server (S) o App (A) ?: ")

try:
    if call_app.lower() == 'a':
        run_application()
    elif call_app.lower() == 's':
        app.run(host='0.0.0.0', debug = False)
except Exception as e:
    print(u"Aplicacion finalizada")