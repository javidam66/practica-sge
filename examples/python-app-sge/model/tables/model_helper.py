# -*- coding: utf-8 -*-

import sqlalchemy, datetime

from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy import Boolean, DateTime, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()