# -*- coding: utf-8 -*-

import sqlalchemy, datetime

from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy import Boolean, DateTime, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker


# Modelo de datos
Base = declarative_base()

from tables.documents import Documents
from tables.links import Links

class Model():
	engine = None
	engine_type = None
	filename = None
	session = None
	tables = {}

	def __init__(self, engine_type, filename = None, debug = False):
		self.engine_type = engine_type
		if engine_type not in ['mysql','postgres']:
			self.filename = filename
			if filename in [None, '']:
				raise Exception("Si no es MySQL o SQLITE\
					 se debe incluir nombre del fichero")
		self.start_engine(debug)

	def start_engine(self, debug = False):
		if self.engine_type:
			self.engine = create_engine(
				self.get_engine(self.engine_type),
				echo = debug
				)
			return True
		else:
			return False

	def get_engine(self, engine_type):
		engine = None
		if engine_type == 'mysql':
			engine = 'mysql://%s:%s@%s/%s' % (
				Settings['DB_USER'],
				Settings['DB_PASS'],
				Settings['DB_HOST'],
				Settings['DB_NAME']
				)
		elif engine_type == 'postgres':
			engine = 'postgresql://%s:%s@%s/%s' % (
				Settings['DB_USER'],
				Settings['DB_PASS'],
				Settings['DB_HOST'],
				Settings['DB_NAME']
				)
		else:
			engine = 'sqlite:///%s.db' % self.filename
		return engine

	def init_database(self):
		Base.metadata.create_all(self.engine)
		Base.metadata.bind = self.engine
		DBSession = sessionmaker(bind=self.engine)
		self.session = DBSession()
		return True